// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';
 
const functions = require('firebase-functions');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');
 
process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
 
exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
 
  function welcome(agent) {
    agent.add(`Welcome to my agent!`);
  }
 
  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
}

  function bodyMassIndex(agent){
    let weight = request.body.queryResult.parameters.weight;
   	let height = request.body.queryResult.parameters.height/100;
   	let bmi = (weight/(height*height)).toFixed(2);
   let result = 'ขออภัยไม่เข้าใจ';
	if (bmi < 18.5) {
   result = 'ผอมไปแล้วนะ กินข้าวบ้าง';
	} else if (bmi >= 18.5 && bmi <= 22.9) {
   result = 'หุ่นอย่างกับนางแบบเลย';
	} else if (bmi >= 23 && bmi <= 24.9) {
   result = 'เริ่มอวบแล้วนะเรา';
	} else if (bmi >= 25 && bmi <= 29.9) {
   result = 'อวบระยะสุดท้ายแล้วนะ';
	} else if (bmi > 30) {
   result = 'อ้วนเกินไปแล้วจ้า ลดอาหารบ้างนะ';
	}
  	agent.add(result);
 }
 
  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('BMI - custom - yes', bodyMassIndex);
  agent.handleRequest(intentMap);
});
