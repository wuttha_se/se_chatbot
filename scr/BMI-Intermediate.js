// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';
 
const functions = require('firebase-functions');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');

const admin = require('firebase-admin');
//admin.initializeApp(functions.config().firebase);
 admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'ws://bmi-intermediate.firebaseio.com/',
});

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
 
exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
 
  function welcome(agent) {
    agent.add(`Welcome to my agent!`);
  }
 
  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
}

  function bodyMassIndex(agent){
    let weight = request.body.queryResult.parameters.weight;
   	let height = request.body.queryResult.parameters.height/100;
   	let bmi = (weight/(height*height)).toFixed(2);
   let result = 'none';
if (bmi < 18.5) {
   result = 'xs';
} else if (bmi >= 18.5 && bmi <= 22.9) {
   result = 's';
} else if (bmi >= 23 && bmi <= 24.9) {
   result = 'm';
} else if (bmi >= 25 && bmi <= 29.9) {
   result = 'l';
} else if (bmi > 30) {
   result = 'xl';
}
    //Cloud firestore
  /*	return admin.firestore().collection('bmi').doc(result).get()
.then(doc => {
    agent.add(doc.data().description);
});*/
  
    
    //Realtime Database
      return admin.database().ref('bmi').child(result).once('value').then(snapshot => {
    agent.add(snapshot.val());
		});
 }
 
  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('BMI - custom - yes', bodyMassIndex);
  agent.handleRequest(intentMap);
});
